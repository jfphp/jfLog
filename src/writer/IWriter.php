<?php

namespace jf\log\writer;

/**
 * Clase para manejar el flujo de salida.
 *
 * @package jfLog
 */
interface IWriter
{
    /**
     * Cierra el flujo de salida.
     */
    public function close();

    /**
     * Inicializa el flujo de salida.
     */
    public function open();

    /**
     * Registra el texto en el flujo de salida.
     *
     * @param string $text Texto a registrar.
     *
     * @return bool|int Cantidad de bytes escritos en el flujo de salida o FALSE si ocurrió un problema.
     */
    public function write(string $text) : bool|int;
}
