<?php

namespace jf\log;

use Exception;
use jf\log\formatter\Formatter;
use jf\log\formatter\IFormatter;
use jf\log\summary\ISummary;
use jf\log\writer\File;
use jf\log\writer\IWriter;
use jf\log\writer\StdErr;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;
use Psr\Log\LogLevel;

/**
 * Clase para manejar el registro de textos.
 * Se apoya en instancias que implementen `jf\log\formatter\IFormatter` y `jf\log\writer\IWriter`.
 *
 * @package jfLog
 */
class Logger extends LogLevel implements LoggerInterface
{
    use LoggerTrait;

    //------------------------------------------------------------------------------
    // Niveles de error.
    //------------------------------------------------------------------------------
    const LEVELS = [
        self::DEBUG     => 100,
        self::INFO      => 200,
        self::NOTICE    => 300,
        self::WARNING   => 400,
        self::ERROR     => 500,
        self::CRITICAL  => 600,
        self::ALERT     => 700,
        self::EMERGENCY => 800
    ];

    /**
     * Directorio por defecto para los registros obtenidos.
     *
     * @var string
     */
    public static string $logdir = '/tmp';

    /**
     * Instancias creadas usando el método `getLogger`.
     *
     * @var static[]
     */
    private static array $_loggers = [];

    /**
     * Nombre de la cookie a usar para desabilitar el registro de información.
     * Para setearla en el navegador:
     *
     * ```
     * document.cookie='JFNOLOG=1;path=/;domain=.joaquinfernandez.net'
     * ```
     *
     * @var string
     */
    public string $cookieName = 'JFLOG';

    /**
     * Formato del mensaje.
     *
     * @var string
     */
    public string $format = "[{timestamp}]\t{name}.{level}\t{message}\t{context}\n";

    /**
     * Formateador de los valores.
     *
     * @var IFormatter|NULL
     */
    public ?IFormatter $formatter = NULL;

    /**
     * Nivel mínimo para registrar información en el flujo de salida.
     *
     * @var int
     */
    public int $minLevel = self::LEVELS[ self::DEBUG ];

    /**
     * Nombre del canal.
     *
     * @var string
     */
    public string $name = '';

    /**
     * Separador de líneas.
     * Permite separar visualmente la información en scripts de consola.
     *
     * @var string
     */
    public string $separator = "--------------------------------------------------------------------------------\n";

    /**
     * Generador del resumen.
     *
     * @var ISummary|NULL
     */
    public ?ISummary $summary = NULL;

    /**
     * Manejador del flujo de salida.
     *
     * @var IWriter|NULL
     */
    public ?IWriter $writer = NULL;

    /**
     * Constructor de la clase.
     *
     * @param string          $name      Nombre a usar para idenficar el registro.
     * @param IWriter|NULL    $writer    Manejador del flujo de salida.
     * @param IFormatter|NULL $formatter Formateador de los valores.
     * @param ISummary|NULL   $summary   Generador del resumen.
     */
    public function __construct(string $name, ?IWriter $writer = NULL, ?IFormatter $formatter = NULL, ?ISummary $summary = NULL)
    {
        if ($name)
        {
            $this->name = $name;
        }
        $this->formatter = $formatter;
        $this->summary   = $summary;
        $this->writer    = $writer;
    }

    /**
     * Destructor de la clase.
     * Muestra el resumen y cierra el stream de salida si se encuentra abierto.
     */
    public function __destruct()
    {
        $_writer = $this->writer;
        if ($this->writer)
        {
            if ($this->summary)
            {
                $_summary = trim((string) $this->summary);
                if ($_summary)
                {
                    try
                    {
                        $this->addSeparator();
                        $_writer->write($_summary . PHP_EOL);
                        $this->addSeparator();
                    }
                    catch (Exception $e)
                    {
                        echo $e->getMessage() . PHP_EOL;
                    }
                }
                $this->summary = NULL;
            }
            $_writer->close();
            $this->writer = NULL;
        }
    }

    /**
     * Registra una línea de texto.
     *
     * @param string $separator Separador a imprimir. Si no se especifica se usa el autogenerado por la clase.
     *
     * @return void
     */
    public function addSeparator(string $separator = '') : void
    {
        $this->write($separator ?: $this->separator);
    }

    /**
     * Construye el contexto para renderizar la plantilla del registro.
     *
     * @param array $context Contexto del mensaje.
     */
    protected function _buildContext(array $context = []) : array
    {
        return [
            'name'      => $this->name,
            'timestamp' => date('Y-m-d H:i:s'),
            ...$context
        ];
    }

    public static function exists(string $name) : bool
    {
        return isset(static::$_loggers[ $name ]);
    }

    /**
     * Devuelve el formateador de los valores.
     *
     * @return IFormatter
     */
    public function getFormatter() : IFormatter
    {
        return $this->formatter ?: ($this->formatter = new Formatter());
    }

    /**
     * Devuelve el logger con el nombre especificado.
     *
     * @param string $name Nombre del logger.
     *
     * @return static
     */
    public static function getLogger(string $name) : static
    {
        if (!static::exists($name))
        {
            static::$_loggers[ $name ] = new static($name);
            if ($name[0] === '/')
            {
                $_dir = dirname($name);
                if ((is_dir($_dir) && is_writable($_dir)) || (is_file($name) && is_writable($name)))
                {
                    static::$_loggers[ $name ]->writer = new File($name);
                }
            }
        }

        return static::$_loggers[ $name ];
    }

    /**
     * Devuelve el logger para el día de hoy con el nombre especificado.
     *
     * @param string $name   Nombre del logger.
     * @param string $format Formato para obtener la fecha.
     *
     * @return static
     */
    public static function getTodayLogger(string $name, string $format = 'Y-m-d') : static
    {
        if (!static::exists($name))
        {
            static::getLogger($name)->writer = new File(sprintf('%s/%s@%s.log', static::$logdir, date($format), $name ?: 'log'));
        }

        return static::$_loggers[ $name ];
    }

    /**
     * Devuelve el manejador del flujo de salida.
     *
     * @return IWriter
     */
    public function getWriter() : IWriter
    {
        return $this->writer ?: ($this->writer = new StdErr());
    }

    /**
     * Registra un mensaje o error.
     *
     * @param mixed  $level   Nivel del mensaje o error a registrar.
     * @param string $message Mensaje a registrar.
     * @param array  $context Contexto del mensaje.
     */
    public function log(mixed $level, $message, array $context = []) : void
    {
        if (empty($_COOKIE[ $this->cookieName ]) && (static::LEVELS[ $level ] ?? 0) >= $this->minLevel)
        {
            $_formatter = $this->getFormatter();
            $_context   = $this->_buildContext(
                [
                    'context' => $context,
                    'level'   => strtoupper($_formatter->format($level)),
                    'message' => $_formatter->format($message)
                ]
            );
            $this->write($_formatter->render($this->format, $_context));
        }
    }

    /**
     * Registra el texto en el flujo de salida.
     *
     * @param string $text Texto a registrar.
     *
     * @return int Cantidad de bytes escritos en el flujo de salida.
     */
    public function write(string $text) : int
    {
        return $this->getWriter()->write($text);
    }
}
