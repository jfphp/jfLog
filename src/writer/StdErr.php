<?php

namespace jf\log\writer;

/**
 * Clase para manejar el flujo de salida por la salida de errores.
 *
 * @package jfLog
 */
class StdErr extends AWriter
{
    /**
     * @inheritdoc
     */
    protected function _openFile() : mixed
    {
        return fopen('php://stderr', 'w');
    }
}
