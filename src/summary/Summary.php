<?php

namespace jf\log\summary;

/**
 * Permite mostrar un resumen al final del registro.
 *
 * @package jfLog
 */
class Summary implements ISummary
{
    /**
     * Resumen a imprimir al final.
     *
     * @var array
     */
    private array $_summary = [];

    /**
     * @inheritdoc
     */
    public function add(string $value, string $name = '')
    {
        if ($name)
        {
            $this->_summary[ $name ] = $value;
        }
        else
        {
            $this->_summary[] = $value;
        }
    }

    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        $length = max(array_map('strlen', array_keys($this->_summary)));
        $lines  = [];
        foreach ($this->_summary as $text => $value)
        {
            $lines[] = str_pad(ucwords(strtolower($text)), $length, ' ', STR_PAD_RIGHT) . ": $value";
        }

        return implode(PHP_EOL, $lines);
    }
}
