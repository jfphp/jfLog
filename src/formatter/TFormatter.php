<?php
/** @noinspection PhpUnused */

declare(strict_types = 1);

namespace jf\log\formatter;

use JsonSerializable;
use Stringable;
use Throwable;

/**
 * Formateador a usar para convertir los diferentes tipos de datos en texto.
 *
 * @package jfLog
 */
trait TFormatter
{
    /**
     * Formatea un array.
     *
     * @param array $array Array a formatear.
     *
     * @return string
     */
    public static function formatArray(array $array) : string
    {
        return json_encode($array, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    /**
     * Formatea un valor booleano.
     *
     * @param bool $bool Boolean Valor a formatear.
     *
     * @return string
     */
    public static function formatBoolean(bool $bool) : string
    {
        return $bool ? '✔' : '×';
    }

    /**
     * Formatea un valor decimal.
     *
     * @param float  $double    Valor a formatear.
     * @param int    $decimals  Cantidad de decimales a mostrar.
     * @param string $decsep    Separador de decimales.
     * @param string $thousands Separador de miles.
     *
     * @return string
     */
    public static function formatDouble(float $double, int $decimals = 2, string $decsep = ',', string $thousands = '.') : string
    {
        return number_format($double, $decimals, $decsep, $thousands);
    }

    /**
     * Formatea una excepción.
     *
     * @param Throwable $exception Excepción a formatear.
     *
     * @return string
     */
    public static function formatException(Throwable $exception) : string
    {
        $_result   = [];
        $_previous = $exception;
        while ($_previous = $_previous->getPrevious())
        {
            $_result[] = static::formatException($_previous);
        }
        $_result[] = sprintf(
            'Excepción %s(%s) - %s at %s:%d',
            $exception::class,
            $exception->getCode(),
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine()
        );

        return implode(', ', $_result);
    }

    /**
     * Formatea un número entero.
     *
     * @param int    $int       Número a formatear.
     * @param string $thousands Separador de miles.
     *
     * @return string
     */
    public static function formatInteger(int $int, string $thousands = '.') : string
    {
        return number_format($int, 0, '', $thousands);
    }

    /**
     * Formatea un valor nulo.
     *
     * @param mixed $null Valor a formatear.
     *
     * @return string
     */
    public static function formatNull(mixed $null) : string
    {
        return $null === NULL ? 'NULL' : '';
    }

    /**
     * Formatea un objeto.
     *
     * @param object $object Objeto a formatear.
     *
     * @return string
     */
    public static function formatObject(object $object) : string
    {
        return match (TRUE)
        {
            $object instanceof Throwable        => static::formatException($object),
            $object instanceof Stringable       => (string) $object,
            $object instanceof JsonSerializable => static::formatValue($object->jsonSerialize()),
            default                             => '[Clase ' . $object::class . ']'
        };
    }

    /**
     * Formatea un recurso.
     *
     * @param resource $resource Recurso a formatear.
     *
     * @return string
     */
    public static function formatResource($resource) : string
    {
        return '[Recurso ' . get_resource_type($resource) . ']';
    }

    /**
     * Formatea un texto.
     *
     * @param string $string Texto a formatear.
     *
     * @return string
     */
    public static function formatString(string $string) : string
    {
        return trim($string);
    }

    /**
     * Formatea un valor de tipo desconocido.
     *
     * @param mixed $unknown Valor a formatear.
     *
     * @return string
     *
     * @noinspection PhpUnusedParameterInspection
     */
    public static function formatUnknownType(mixed $unknown) : string
    {
        return '[Tipo desconocido]';
    }

    /**
     * Formatea un valor de cualquier tipo.
     *
     * @param mixed $value Valor a formatear.
     *
     * @return string
     */
    public static function formatValue(mixed $value) : string
    {
        //Los valores posibles de `gettype` son:
        // array, boolean, double, integer, object, resource, string, "unknown type", NULL
        $_formatter = static::class . '::format' . str_replace(' ', '', mb_convert_case(gettype($value), MB_CASE_TITLE));

        return $_formatter($value);
    }
}
