# jfLog

Permite disponer de un sistema para controlar el registro de información.
Implementa la interfaz `Psr\Log\LoggerInterface` por lo que puede ser usado para 
reemplazar paquetes que son demasiado pesados para realizar una tarea tan simple.

Para obtener un logger se puede crear una instancia o usar el método `getLogger`
el cual permite tener varios loggers durante la ejecución de la aplicación y ser
recuperados desde cualquier parte del código ya que la llamada a este método 
devuelve siempre la misma instancia para el nombre especificado.

# Ejemplos

```php
use jf\log\Logger;
use jf\log\writer\File;
use jf\log\writer\StdOut;

//-----------------------------------------------------------------------------
// Asignando el nombre del archivo de salida.
//-----------------------------------------------------------------------------
$filename    = sprintf('/tmp/%s-MiWeb.log', date('Y-m-d'));

// Usando un logger como singleton
$log         = Logger::getLogger('MiWeb');
$log->writer = new File('/tmp/test-logger.log');

// Creando una instancia
$log         = new Logger('MiWeb', new File('/tmp/test-logger.log'));

// Usando el nombre del archivo agrega un writer File
// si se puede escribir en el directorio
$log         = Logger::getLogger($filename);

// Devuelve una instancia que gestiona el archivo /tmp/Y-m-d@sql.log
$log         = Logger::getTodayLogger('sql');

//-----------------------------------------------------------------------------
// Mostrando la salida por pantalla en vez de un archivo.
//-----------------------------------------------------------------------------
// Usando un singleton
$log         = Logger::getLogger('MiWeb');
// Creando una instancia
$log         = new Logger('MiWeb');
$log->writer = new StdOut();

//-----------------------------------------------------------------------------
// Agregando trazas.
//-----------------------------------------------------------------------------
$log->debug('Texto de depuración');
$log->warning('Texto de aviso');

```
