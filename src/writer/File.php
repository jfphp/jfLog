<?php

namespace jf\log\writer;

use jf\assert\Assert;

/**
 * Clase para manejar el flujo de salida usando un archivo.
 *
 * @package jfLog
 */
class File extends AWriter
{
    /**
     * Máscara a usar al momento de crear el archivo si no existe.
     *
     * @var int|null
     */
    public ?int $umask = NULL;

    /**
     * Nombre del archivo donde se escribirá el registro.
     *
     * @var string
     */
    public string $filename = '/dev/null';

    /**
     * Tipo de acceso requerido en el flujo de salida.
     *
     * @var string
     */
    public string $mode = 'a+';

    /**
     * Constructor de la clase.
     *
     * @param string $filename Nombre del archivo donde se escribirá el registro.
     * @param string $mode     Tipo de acceso requerido en el flujo de salida.
     */
    public function __construct(string $filename = '/dev/null', string $mode = 'a+')
    {
        if ($filename)
        {
            $this->filename = $filename;
        }
        if ($mode)
        {
            $this->mode = $mode;
        }
    }

    /**
     * @inheritdoc
     */
    protected function _openFile() : mixed
    {
        $_filename = $this->filename;
        $_outdir   = dirname($_filename);
        if (!str_contains($_filename, '://'))
        {
            if (!is_dir($_outdir))
            {
                Assert::mkdir($_outdir, 0755, TRUE);
            }
            if (is_file($_filename))
            {
                Assert::isWritable($_filename);
            }
            else
            {
                Assert::isWritable($_outdir);
            }
        }
        $_umask = $this->umask;
        if ($_umask !== NULL)
        {
            $_umask = umask($_umask);
        }
        $_fp = fopen($_filename, $this->mode);
        if ($_umask !== NULL)
        {
            umask($_umask);
        }
        Assert::isResource($_fp, 'No se pudo crear el archivo {0}', $_filename);

        return $_fp;
    }
}
