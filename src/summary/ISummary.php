<?php

namespace jf\log\summary;

use Stringable;

/**
 * Permite mostrar un resumen al final del registro.
 * Sirve para los procesos que se ejecutan en consola y van acumulando
 * registros estadísticos que se suelen mostrar al final.
 *
 * @package jfLog
 */
interface ISummary extends Stringable
{
    /**
     * Registra una entrada en el resumen.
     *
     * @param string $value Valor de la entrada.
     * @param string $name  Nombre de la entrada.
     */
    public function add(string $value, string $name = '');
}
