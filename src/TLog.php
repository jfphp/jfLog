<?php

namespace jf\log;

use jf\log\writer\File;
use Psr\Log\LoggerInterface;

/**
 * Agrega la funcionalidad de un logger a la clase.
 *
 * @package jfLog
 */
trait TLog
{
    /**
     * Nombre del archivo de log.
     *
     * @var string|NULL
     */
    protected ?string $_logFilename = NULL;

    /**
     * Modo de apertura del archivo de log.
     *
     * @var string
     */
    protected string $_logFileMode = 'a+';

    /**
     * Instancia del logger.
     *
     * @var LoggerInterface|NULL
     */
    private ?LoggerInterface $_logger = NULL;

    /**
     * Inicializa el trait.
     */
    protected function _initLogger()
    {
        $_logFilename = $this->_logFilename;
        if ($_logFilename)
        {
            $_class        = explode('\\', get_class($this));
            $this->_logger = new Logger(
                end($_class),
                new File($_logFilename, $this->_logFileMode)
            );
        }
    }

    /**
     * Devuelve el logger siendo usado.
     *
     * @return Logger|NULL
     */
    public function getLogger() : ?LoggerInterface
    {
        return $this->_logger;
    }

    /**
     * Agrega una entrada al registro.
     *
     * @param array $args Parámetros a pasar al método `log` del logger.
     */
    public function log(...$args)
    {
        $this->_logger?->log(...$args);
    }

    /**
     * Asigna el logger a usar.
     *
     * @param LoggerInterface|NULL $logger Logger a asignar.
     */
    public function setLogger(?LoggerInterface $logger)
    {
        $this->_logger = $logger;
    }
}
