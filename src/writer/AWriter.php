<?php

namespace jf\log\writer;

use jf\assert\Assert;

/**
 * Clase base para los flujos de salida sencillos.
 *
 * @package jfLog
 */
abstract class AWriter implements IWriter
{
    /**
     * Puntero del flujo de salida.
     *
     * @var resource|NULL
     */
    private $_file = NULL;

    /**
     * @inheritdoc
     */
    public function close()
    {
        if ($this->_file)
        {
            fclose($this->_file);
            $this->_file = NULL;
        }
    }

    /**
     * @inheritdoc
     */
    public function open()
    {
        $_file = $this->_file;
        if (!$_file)
        {
            $_file = $this->_openFile();
            Assert::isResource($_file, 'No se pudo abrir el flujo de salida');
            $this->_file = $_file;
        }

        return $_file;
    }

    /**
     * Devuelve el flujo de salida que se usará para escribir las trazas.
     * Si se devuelve un `string` se lanza una excepción con ese mensaje.
     *
     * @return resource|string
     */
    abstract protected function _openFile() : mixed;

    /**
     * @inheritdoc
     */
    public function write(string $text) : bool|int
    {
        return fwrite($this->open(), $text);
    }
}
