<?php

namespace jf\log\writer;

/**
 * Clase para manejar el flujo de salida por la salida estándar.
 *
 * @package jfLog
 */
class StdOut extends AWriter
{
    /**
     * @inheritdoc
     */
    protected function _openFile() : mixed
    {
        return fopen('php://stdout', 'w');
    }
}
