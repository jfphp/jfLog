<?php

namespace jf\log\formatter;

/**
 * Interfaz para los formateadores a usar que convertirán los diferentes tipos de datos en texto.
 *
 * @package jfLog
 */
interface IFormatter
{
    /**
     * Formatea un valor de cualquier tipo.
     *
     * @param mixed $value Valor a formatear.
     *
     * @return string Representación textual del valor.
     */
    public function format(mixed $value) : string;

    /**
     * Renderiza la plantilla con las variables especificadas en el contexto.
     *
     * @param string $tpl     Plantilla de la traza.
     * @param array  $context Contexto a usar para reemplazar las variables existentes en la plantilla.
     *                        AVISO: Los valores usados son eliminados de esta variable.
     *
     * @return string
     */
    public function render(string $tpl, array &$context = []) : string;
}
