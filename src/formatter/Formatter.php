<?php
/** @noinspection PhpUnused */

namespace jf\log\formatter;

/**
 * Formateador a usar para convertir los diferentes tipos de datos en texto.
 *
 * @package jfLog
 */
class Formatter implements IFormatter
{
    use TFormatter;

    /**
     * Cantidad de decimales a mostrar cuando se formatean número decimales.
     *
     * @var int
     */
    public int $decimals = 2;
    /**
     * Separador de la parte decimal.
     *
     * @var string
     */
    public string $decimalSeparator = ',';
    /**
     * Separador de miles a usar.
     *
     * @var string
     */
    public string $thousandSeparator = '.';

    /**
     * Formatea un valor de cualquier tipo.
     *
     * @param mixed $value Valor a formatear.
     *
     * @return string
     */
    public function format(mixed $value) : string
    {
        return match (TRUE)
        {
            is_double($value) => $this->_formatDouble($value),
            is_int($value)    => $this->_formatInteger($value),
            default           => static::formatValue($value)
        };
    }

    /**
     * Formatea un valor decimal.
     *
     * @param float $double Valor a formatear.
     *
     * @return string
     */
    protected function _formatDouble(float $double) : string
    {
        return static::formatDouble($double, $this->decimals, $this->decimalSeparator, $this->thousandSeparator);
    }

    /**
     * Formatea un número entero.
     *
     * @param int $int Integer number to format.
     *
     * @return string
     */
    protected function _formatInteger(int $int) : string
    {
        return static::formatInteger($int, $this->thousandSeparator);
    }

    /**
     * @inheritdoc
     */
    public function render(string $tpl, array &$context = []) : string
    {
        return preg_replace_callback(
            '/{([^{}]+)}/',
            function ($var) use (&$context)
            {
                $_key = trim($var[1]);
                if (array_key_exists($_key, $context))
                {
                    $_value = $context[ $_key ];
                    $_value = $_key === 'context' && !$_value
                        ? ''
                        : $this->format($_value);
                    unset($context[ $_key ]);
                }
                else
                {
                    $_value = '';
                }

                return $_value;
            },
            $tpl
        );
    }
}
