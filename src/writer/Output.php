<?php

namespace jf\log\writer;

/**
 * Clase para manejar el flujo de salida enviado al navegador.
 *
 * @package jfLog
 */
class Output extends AWriter
{
    /**
     * @inheritdoc
     */
    protected function _openFile() : mixed
    {
        return fopen('php://output', 'w');
    }
}
